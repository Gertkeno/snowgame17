#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL_image.h>

#include <GameManager.h>
#include <Controller.h>

#include <iostream>
#include <FrameTime.h>

void test();
bool open_assets();
void close_assets();

Controller gControls;

int main( int argc, char** argv )
{
	atexit( [](){ std::cerr << std::endl; } ); //flush cout messages
	if( SDL_Init( SDL_INIT_EVERYTHING ) > 0 )
	{
		std::cerr << "[main]  SDL_Init failed: " << SDL_GetError();
		return 1;
	}
	static constexpr auto IMG_FLAGS = IMG_INIT_JPG | IMG_INIT_PNG;
	if( (IMG_Init( IMG_FLAGS ) & IMG_FLAGS) != IMG_FLAGS )
	{
		std::cerr << "[main]  IMG_Init failed: " << IMG_GetError();
		return 2;
	}

	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

	SDL_Window * mainWindow = SDL_CreateWindow( "Working Title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE );

	SDL_GLContext mainContext = SDL_GL_CreateContext( mainWindow );

	glewExperimental = GL_TRUE;
	if( glewInit() > 0 )//init after context creation
	{
		std::cerr << "[main]  glewInit failed: " << glewGetErrorString( glGetError() );
		return 4;
	}

	glEnable( GL_DEPTH_TEST );
	{
		int w, h;
		SDL_GetWindowSize( mainWindow, &w, &h );
		glViewport( 0, 0, w, h );
		//use SDL_Event->window.data1 for width and data2 for height when getting resize
	}

	if( SDL_GL_SetSwapInterval( -1 ) != 0 )//vsync
	{
		std::cerr << "[main]  Late swap not supported.\n";
		SDL_GL_SetSwapInterval( 1 );
	}

	/*if( SDL_NumJoysticks() > 0 )
	{
		gControls.joy_add( 0 );
	}*/

	if( not gControls.load_file( "control.ini" ) )
	{
		std::cerr << "[main]  control.ini not loaded, using defaults\n";
		std::pair<std::string, Input> def[]{
			{"CONFIRM", {Input::type_t::KEYBOARD, SDLK_RETURN}},
			{"LEFT",    {Input::type_t::KEYBOARD, SDLK_a}},
			//{"LEFT",    {Input::type_t::JOYAXIS, 0, -22222}},
			{"RIGHT",   {Input::type_t::KEYBOARD, SDLK_d}},
			//{"RIGHT",   {Input::type_t::JOYAXIS, 0, 22222}},
			{"DOWN",    {Input::type_t::KEYBOARD, SDLK_s}},
			{"UP",      {Input::type_t::KEYBOARD, SDLK_w}},
			{"PLACE",   {Input::type_t::MOUSE, SDL_BUTTON_LEFT}},
			{"ROTX",    {Input::type_t::KEYBOARD, SDLK_LEFT}},
			{"ROTY",    {Input::type_t::KEYBOARD, SDLK_RIGHT}},
			{"JUMP",    {Input::type_t::KEYBOARD, SDLK_SPACE}},
			{"BUTTONS", {Input::type_t::JOYBUTTON, SDL_CONTROLLER_BUTTON_A}},
		};
		gControls.load_raw( def, sizeof(def)/sizeof(*def) );
	}
	if( not open_assets() )
	{
		std::cerr << "[main]  Couldn't open assets\n";
		return 5;
	}
	GameManager::init();

	test();

	static constexpr float MAX_FPS( 1000.0/90.0f );
	while( GameManager::get_game_state() != GameManager::QUITTING )
	{
		FrameTime::update();

		//update here
		GameManager::update();
		glClearColor( 0.8f, 0.8f, 0.8f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		GameManager::draw();
		SDL_GL_SwapWindow( mainWindow );

		auto su = FrameTime::since_update();
		if( su < MAX_FPS )
		{
			SDL_Delay( MAX_FPS - su );
		}
	}

	gControls.save_file( "sontrol.ini" );//TODO: change to control.ini
	GameManager::destroy();
	close_assets();
	SDL_GL_DeleteContext( mainContext );
	SDL_DestroyWindow( mainWindow );
	SDL_Quit();
	return EXIT_SUCCESS;
}
