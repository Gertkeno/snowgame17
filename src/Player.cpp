#include <Player.h>

#include <Controller.h>
#include <FrameTime.h>
#include <geo/Functional.h>
#include <Stage.h>

#include <iostream>

extern Controller gControls;

namespace
{
constexpr float T{ M_PI*2 };
constexpr float X_MOVEMENT_BASE{0.2f};
constexpr float Y_MOVEMENT_BASE{0.0f};
constexpr float Y_JUMP{ 0.3f };
constexpr float GRAVITY{ 0.87f };
constexpr float STARTING_GRAVITY{ GRAVITY*0.1f };
constexpr float TERMINAL_VELOCITY{ GRAVITY * 1.1f };

constexpr float X_FALLING{ X_MOVEMENT_BASE * 0.2f };
constexpr float HEIGHT{ Player::SCALE * 0.95f };
}

Player::Player()
{
	_status = FALLING;
}

Player::~Player(){}

void Player::update()
{
	float horizontalMovement{ X_MOVEMENT_BASE };
	switch( _status )
	{
		case CLIMBING:
		case FALLING:
			horizontalMovement = X_FALLING;
			break;
		case GROUNDED:
		case TOTAL:
			break;
	}

	float a;
	float x{0}, y{0};
	if( (a = gControls["LEFT"]) > 0.0f )
		x = -a * horizontalMovement;
	else if( (a = gControls["RIGHT"]) > 0.0f )
		x = a * horizontalMovement;

	_velocity.y -= GRAVITY * FrameTime::get();
	if( _velocity.y < -TERMINAL_VELOCITY )
	{
		_velocity.y = -TERMINAL_VELOCITY;
	}

	_worldPos.x += (_velocity.x + x) * FrameTime::get();
	_worldPos.y += (_velocity.y + y) * FrameTime::get();

	geometry::Circle pos{ {_worldPos.x, _worldPos.y}, HEIGHT };
	auto force = Stage::get_force(pos);
	//std::cerr << "Force x " << force.x << ", y " << force.y << std::endl;
	if( force.x != 0.0f or force.y != 0.0f )
	{
		if( force.y > std::abs(force.x) )
			_status = GROUNDED;
		else if ( std::abs( force.x ) > -force.y )
			;//walljump?
		_velocity = { 0.0f, -STARTING_GRAVITY };
		if( std::abs( force.x ) > 0.002f )
			_worldPos.x += force.x;
		_worldPos.y += force.y;
	}
	//debug reset
	else if( _worldPos.y < -1.0f )
	{
		_status = GROUNDED;
		_velocity = {0.0f,-STARTING_GRAVITY};
		_worldPos.x = 0.0f;
		_worldPos.y = 0.9f;
	}

	if( gControls("JUMP") and _status != FALLING )
	{
		_status = FALLING;
		_velocity.y = Y_JUMP;
		_velocity.x = x;
	}

	if( (a = gControls["ROTX"]) > 0.0f )
		_turn.x += a*T * FrameTime::get();
	if( (a = gControls["ROTY"]) > 0.0f )
		_turn.y += a*T * FrameTime::get();

	if( gControls["CONFIRM"] == 1.0f )
	{
		_turn.x = 0.0f;
		_turn.y = 0.0f;
	}
}

void Player::draw()
{
}

void Player::start( float x, float y )
{
	_velocity = glm::vec2();
	_worldPos.x = x;
	_worldPos.y = y;
	_status = FALLING;
}
