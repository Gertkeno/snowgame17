#include <info/Shaders.h>
#include <info/Models.h>

#include <iostream>
gert::Shader* gShade;
gert::Wavefront* gModel;

const auto object_frag = R"(#version 330 core

in vec2 TexCoord;

in vec3 nerm;

uniform float dark;
uniform vec4 colorMod;
uniform sampler2D tex0;

void main()
{
	vec4 color = texture2D( tex0, TexCoord );
	gl_FragColor = colorMod * color * dark;
})";

const auto object_vert = R"(#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 vnormal;

out vec2 TexCoord;
out vec3 nerm;

uniform float screenXOverY;
uniform mat4 model;
uniform mat4 normMod;
uniform sampler2D heightMap;

void main()
{
	vec4 h = texture2D( heightMap, texcoord );
	TexCoord = vec2(texcoord.x, 1 - texcoord.y);
	nerm = vnormal;

	vec4 final = model * vec4( position, 1.0 );
	final.y *= screenXOverY;
	final.z -= h.r;
	gl_Position = final;
})";

bool open_assets()
{
	gShade = new gert::Shader;

	if(gShade->open_shader( object_vert, gert::Shader::VERTEX ))
	{
		if(gShade->open_shader( object_frag, gert::Shader::FRAGMENT ))
		{
			gShade->attach_shaders();
			gShade->use_shader();
		}
	}

	gert::Wavefront::update_attribs();

	gModel = new gert::Wavefront[model::TOTAL];
	extern float untitled_vertices;
	extern unsigned untitled_len;
	gModel[model::TEST_MONKEY].raw_verts_load( &untitled_vertices, untitled_len );
	extern float cube_vertices;
	extern unsigned cube_len;
	gModel[model::CUBE].raw_verts_load( &cube_vertices, cube_len );
	extern float plane_vertices;
	extern unsigned plane_len;
	gModel[model::PLANE].raw_verts_load( &plane_vertices, plane_len );

	return true;
}

void close_assets()
{
	delete gShade;
	delete[] gModel;
}
