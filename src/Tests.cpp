#include <geo/Functional.h>

#include <iostream>

void test()
{
	return;
	geometry::Line N{ {-1.0f,-1.0f}, {-0.999f,1.0f} };
	geometry::Line A{ {-1.0f, -1.0f}, {1.0f, 0.0f} };
	std::cerr << N.slope() << ", " << A.slope() << '\n';
	return;
	geometry::Circle B{ {6, 5}, 5 };

	std::cerr << A.slope() << '\n';

	auto temp = geometry::intersection_force( B, A );
	std::cerr << temp.x << ", " << temp.y;
}
