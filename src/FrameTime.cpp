#include <FrameTime.h>
#include <SDL2/SDL_timer.h>

namespace FrameTime
{
	namespace
	{
		double _lastFrameTime;
		unsigned _frameStart;
	}

	double get()
	{
		return _lastFrameTime;
	}

	void update()
	{
		static constexpr double MS_CONVERTER{1.0/1000.0};
		_lastFrameTime = since_update() * MS_CONVERTER;
		_frameStart = SDL_GetTicks();
	}

	unsigned since_update()
	{
		return SDL_GetTicks() - _frameStart;
	}
}
