#include <geo/Line.h>
#include <cmath>
#include <algorithm>

namespace geometry
{
	float Line::length() const
	{
		return sqrt( pow( a.x - b.x, 2 ) + pow( a.y - b.y, 2 ) );
	}

	float Line::slope() const
	{
		auto xdiff = a.x - b.x;
		if( xdiff == 0.0f ) return xdiff;
		return (a.y - b.y) / xdiff;
	}

	float Line::y_offset() const
	{
		//works with A or B
		return a.y - (slope() * a.x);
	}

	float Line::high_x() const
	{
		return std::max( a.x, b.x );
	}

	float Line::low_x() const
	{
		return std::min( a.x, b.x );
	}

	float Line::high_y() const
	{
		return std::max( a.y, b.y );
	}

	float Line::low_y() const
	{
		return std::min( a.y, b.y );
	}

	float Line::height( float x ) const
	{
		return slope() * x + y_offset();
	}

	bool Line::y_parallel() const
	{
		return (a.x - b.x) == 0.0f;
	}

	bool Line::x_parallel() const
	{
		return (a.y - b.y) == 0.0f;
	}
}
