#include <geo/Functional.h>
#include <iostream>

namespace geometry
{
	glm::vec2 line_intersection( const Line& a, const Line& b )
	{
		//x coordinate of intersection
		auto x = (b.y_offset() - a.y_offset()) / (a.slope() - b.slope());
		//height gets Y based on x input
		return {x, a.height( x )};
	}

	glm::vec2 intersection_force( const Circle& a, const Line& w )
	{
		if( a.pos.x + a.radius <  w.low_x() or a.pos.x - a.radius > w.high_x() or a.pos.y + a.radius < w.low_y() or a.pos.y - a.radius > w.high_y() ) return glm::vec2();
		//slope is 0 if parallel to x or y
		if( w.slope() == 0.0f )
		{
			float diff;
			glm::vec2 norm;
			if( w.y_parallel() )
			{
				diff = a.pos.x - w.a.x;
				//std::cerr << "Got y parralel force of " << dist << '\n';
				norm.x = 1.0f;
			}
			else if( w.x_parallel() )
			{
				diff = a.pos.y - w.a.y;
				//std::cerr << "Got x parralel force of " << dist << '\n';
				norm.y = 1.0f;
			}
			auto dist = a.radius - std::abs( diff );
			return norm * std::max( 0.0f, dist );
		}

		//any other situation
		glm::vec2 closePoint = { a.pos.x, w.height( a.pos.x ) };

		auto dist = distance<float>( closePoint, a.pos );
		if( dist > a.radius ) return glm::vec2();
		auto s = w.slope();
		glm::vec2 attemptedNormal {-s, 1.0f - s};
		attemptedNormal = glm::normalize( attemptedNormal );
		return attemptedNormal * (a.radius-dist);
	}
}
