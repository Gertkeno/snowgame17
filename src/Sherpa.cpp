#include <Sherpa.h>

#include <Wavefront.h>
#include <Shader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <info/Models.h>

Sherpa::Sherpa(){}
Sherpa::~Sherpa(){}

void Sherpa::update()
{
	Player::update();
}

void Sherpa::draw()
{
	glm::mat4 pos = glm::translate( glm::mat4(), _worldPos );
	pos = glm::scale( pos, glm::vec3(SCALE) );
	pos = glm::rotate( pos, _turn.x, { 1.0f, 0.0f, 0.0f } );
	pos = glm::rotate( pos, _turn.y, { 0.0f, 1.0f, 0.0f } );

	gert::Shader::Active_Shader->set_uniform( "dark", _worldPos.x+1 );
	gModel[model::TEST_MONKEY].draw( pos, color::GREEN );
	gert::Shader::Active_Shader->set_uniform( "dark", 1.0f );
}
