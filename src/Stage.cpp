#include <Stage.h>

#include <vector>
#include <SDL2/SDL_rwops.h>
#include <Texture.h>
#include <info/Models.h>
#include <geo/Functional.h>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
namespace Stage
{
	namespace
	{
		std::vector<geometry::Line> _walls;
		gert::Texture _background;
		glm::vec2 _scale;
	}

	bool load_stage( const void * background, size_t bgsize, const geometry::Line * data, size_t size, glm::vec2 scale )
	{
		_scale = scale;
		if( data == nullptr or background == nullptr) return false;
		
		if( not _background.RW_load( background, bgsize ) ) return false;

		const auto w = _background.get_width()/2;
		const auto h = _background.get_height()/2;
		const auto recW = 1.0f/w;
		const auto recH = 1.0f/h;

		for( auto i = 0u; i < size; ++i )
		{
			geometry::Line m{ data[i] };
			m.a.y = h*2 - m.a.y - h;
			m.b.y = h*2 - m.b.y - h;
			//m.a.y -= h;
			//m.b.y -= h;
			m.a.x -= w;
			m.b.x -= w;

			m.a.x *= recW * _scale.x;
			m.a.y *= recH * _scale.y;
			m.b.x *= recW * _scale.x;
			m.b.y *= recH * _scale.y;
			_walls.push_back( m );
		}

		return true;
	}

	glm::vec2 get_force( const geometry::Circle& pos )
	{
		glm::vec2 total;
		if( _walls.size() == 0 ) return total;
		//total = geometry::intersection_force( pos, _walls[2] );
		//return total;
		for( auto &i: _walls )
		{
			//total = geometry::intersection_force( pos, i );
			auto f = geometry::intersection_force( pos, i );
			if( i.flip ) total -= f;
			else total += f;
			/*if( std::abs( total.x ) > 0.0f or std::abs( total.y ) > 0.0f )
			{
				//std::cerr << "hit point a " << i.a.x << ", " << i.a.y << std::endl;
				return total;
			}*/
		}

		return total;
	}

	void draw()
	{
		_background.set_active();
		glm::mat4 d = glm::scale( glm::mat4(), { _scale.x, _scale.y, 1.0f } );
		gModel[model::PLANE].draw( glm::translate( d, {0.0f,0.0f,0.9999f} ), color::WHITE );
	}
}
