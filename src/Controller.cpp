#include <Controller.h>
#include <SDL2/SDL.h>
#include <fstream>
#include <algorithm>
#include <iostream>

namespace
{
	//normalized values
	constexpr float AXIS_PRESS_THRESHOLD{ 0.8f };
	constexpr float AXIS_RESET_THRESHOLD{ 0.7f };
	constexpr float AXIS_DEAD_ZONE{ 0.039f };

	void digital_press(Input* t)
	{
		if( t->held < 1.0f )
			t->framePress = true;
		t->held = 1.0f;
		//std::cerr << "Pressed button " << t->button << std::endl;
	}

	void digital_release(Input* t)
	{
		t->held = 0.0f;
	}

	constexpr auto NAMES={"KEYBOARD","JOYBUTTON","JOYAXIS","MOUSE","NONE"};
	constexpr auto type_to_name( Input::type_t e )
	{
		return NAMES.begin()[static_cast<int>(e)];
	}
	constexpr auto name_to_type( const char * n )
	{
		char i{0};
		for( auto &v: NAMES )
		{
			if( strcmp( n, v ) == 0 ) return static_cast<Input::type_t>(i);
			++i;
		}
		return Input::type_t::NONE;
	}
}

Controller::Controller() {}
Controller::~Controller() {}

void Controller::manage_inputs( const SDL_Event* e )
{
	auto stateDigital = digital_release;
	//get press/release, if axis apply and return
	switch( e->type )
	{
		case SDL_JOYBUTTONDOWN:
			//only joysticks need to be tested, return if not tracked
			if( not _tracked_joystick( e->jbutton.which ) ) return;
		case SDL_KEYDOWN:
		case SDL_MOUSEBUTTONDOWN:
			stateDigital = digital_press;
			break;
		case SDL_JOYAXISMOTION:
			if( not _tracked_joystick( e->jaxis.which ) ) return;
			_search_input( Input::type_t::JOYAXIS, e->jaxis.axis, [e](Input* t)
			{
				if( t->axisMax == 0 ) return;
				t->held = float(e->jaxis.value) / t->axisMax;
				if( t->held < AXIS_DEAD_ZONE ) t->held = 0.0f;
				else if( t->held > 1.0f ) t->held = 1.0f;

				if( t->axisPass and t->held < AXIS_RESET_THRESHOLD )
					t->axisPass = false;
				else if( not t->axisPass and t->held > AXIS_PRESS_THRESHOLD )
				{
					t->framePress = true;
					t->axisPass = true;
				}
				//std::cerr << "Axis " << t->button << " value " << t->held << std::endl;
			});
			return;
	}

	//apply digital press/release
	switch( e->type )
	{
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			_search_input( Input::type_t::KEYBOARD, e->key.keysym.sym, stateDigital );
			break;
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
			_search_input( Input::type_t::MOUSE, e->button.button, stateDigital );
			break;
		case SDL_JOYBUTTONUP:
		case SDL_JOYBUTTONDOWN:
			_search_input( Input::type_t::JOYBUTTON, e->jbutton.button, stateDigital );
			break;
	}
}

float Controller::get_input( std::string name ) const
{
	if( _data.count( name ) == 0 )
	{
		std::cerr << "[Controller]  Control held " << name << " not found\n";
	}
	return _data.at(name).held;
}

float Controller::operator[]( std::string name ) const
{
	return get_input(name);
}

bool Controller::get_press( std::string name ) const
{
	if( _data.count( name ) == 0 )
	{
		std::cerr << "[Controller] Control press " << name << " not found\n";
	}
	return _data.at(name).framePress;
}

bool Controller::operator()( std::string name ) const
{
	return get_press( name );
}

void Controller::_search_input( Input::type_t t, int id, std::function<void(Input*)> f )
{
	for( auto & i: _data )
	{
		if( i.second.myType != t ) continue;
		if( i.second.button != id ) continue;
				
		f( &i.second );
	}
}

void Controller::reset_press()
{
	for( auto &i: _data )
		i.second.framePress = false;
}

void Controller::reset_held()
{
	for( auto &i: _data )
		i.second.held = 0.0f;
}

bool Controller::save_file( const char * filename ) const
{
	if( filename == nullptr ) return false;
	std::ofstream out( filename );
	if( not out.is_open() )
	{
		std::cerr << "[Controller]  Couldn't open file " << filename << '\n';
		return false;
	}

	for( auto & i: _data )
	{
		out << i.first;
		out << ' ';
		out << type_to_name( i.second.myType );
		out << ' ';
		out << i.second.button;
		out << ' ';
		out << i.second.axisMax;

		out << '\n';
	}
	return true;
}

bool Controller::load_file( const char * filename )
{
	if( filename == nullptr ) return false;
	std::ifstream in( filename );
	if( not in.is_open() )
	{
		std::cerr << "[Controller]  Couldn't open file " << filename << '\n';
		return false;
	}

	while( not in.eof() )
	{
		std::string name;
		in >> name;
		if( name.empty() ) break;
		std::string type;
		in >> type;
		if( type.empty() ) break;
		long value;
		in >> value;
		int axis;
		in >> axis;

		Input foo;
		foo.myType = name_to_type( type.c_str() );
		foo.button = value;
		foo.axisMax = axis;

		_data[name] = foo;
	}
	return true;
}

bool Controller::load_raw( std::pair<std::string, Input> * data, size_t s )
{
	_data.clear();

	for( auto a = 0u; a < s; ++a )
	{
		_data[data[a].first] = data[a].second;
	}
	return true;
}

bool Controller::mutate_value( std::string key, Input val )
{
	if( _data.count( key ) == 0 )
	{
		std::cerr << "[Controller]  Mutating " << key << " key not found\n";
		return false;
	}
	_data[key] = val;
	reset_held();
	reset_press();
	return true;
}

bool Controller::_tracked_joystick( SDL_JoystickID foo )
{
	for( auto &i: _joys )
		if( SDL_JoystickInstanceID( i ) == foo ) return true;
	return false;
}

bool Controller::joy_add( int device_index )
{
	auto foo{ SDL_JoystickOpen( device_index ) };
	if( foo == nullptr ) return false;
	_joys.push_back( foo );
	std::cerr << "[Controller]  Opened joystick " << device_index << '\t' << SDL_JoystickName( foo ) << '\n';
	reset_held();
	return true;
}

bool Controller::joy_remove( SDL_JoystickID instance_id )
{
	for( auto i = 0u; i < _joys.size(); ++i )
	{
		if( instance_id != SDL_JoystickInstanceID( _joys[i] ) ) continue;
		std::cerr << "[Controller]  Close joystick " << instance_id << '\t' << SDL_JoystickName( _joys[i] ) << '\n';
		SDL_JoystickClose( _joys[i] );
		_joys.erase( _joys.begin()+i );
		reset_held();
		return true;
	}
	return false;
}

int Controller::joy_count() const
{
	return _joys.size();
}
