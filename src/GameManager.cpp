#include <GameManager.h>

#include <GL/glew.h>
#include <Controller.h>
#include <SDL2/SDL.h>
#include <Sherpa.h>
#include <Wavefront.h>
#include <Shader.h>
#include <iostream>
#include <Stage.h>

extern Controller gControls;
		extern unsigned char cavetest_jpeg;
		extern unsigned int  cavetest_jpeg_len;
namespace GameManager
{
	void _update_shader();
	namespace
	{
		state_t _gameState;
		SDL_Event _event;

		Sherpa _me;
		float _screenXOverY;
	}

	state_t get_game_state()
	{
		return _gameState;
	}

	void update()
	{
		gControls.reset_press();
		while( SDL_PollEvent( &_event ) )
		{
			gControls.manage_inputs( &_event );

			switch( _event.type )
			{
				case SDL_QUIT:
					_gameState = QUITTING;
					break;
				case SDL_JOYBUTTONDOWN:
					//std::cerr << int(_event.jbutton.button) << '\n';
					break;
				case SDL_KEYDOWN:
					switch( _event.key.keysym.sym )
					{
						case SDLK_F4:
							if( SDL_GetModState() & KMOD_ALT )
								_gameState = QUITTING;
							break;
					}
					break;
				case SDL_WINDOWEVENT:
					if( _event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED )
					{
						glViewport( 0, 0, _event.window.data1, _event.window.data2 );
						_screenXOverY = _event.window.data1/float(_event.window.data2);
						gert::Shader::Active_Shader->set_uniform( "screenXOverY", _screenXOverY );
					}
					break;
				case SDL_JOYDEVICEADDED:
					gControls.joy_add( _event.jdevice.which );
					break;
				case SDL_JOYDEVICEREMOVED:
					gControls.joy_remove( _event.jdevice.which );
					break;
			}
		}
		_me.update();
	}

	void draw()
	{
		Stage::draw();
		_me.draw();
	}

	void init()
	{
		_me.start( 0.0f, 0.5f );
		_gameState = PLAYING;
		geometry::Line l[] {{{0, 205}, {194, 185}},
			{{60, 205}, {40, 33}},
			{{340, 140}, {490, 140}, true},
			{{194, 185}, {392, 217}},
			{{392, 217}, {479, 224}}};
		static constexpr glm::vec2 a{ 1.0f, 9.0f/16.0f };
		Stage::load_stage( &cavetest_jpeg, cavetest_jpeg_len, l, sizeof(l)/sizeof(*l), a );
	}

	void destroy()
	{
	}

	void _update_shader()
	{
		gert::Wavefront::update_attribs();
		gert::Shader::Active_Shader->set_uniform( "screenYOverX", _screenXOverY );
	}
}
