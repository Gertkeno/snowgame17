#include "Collision.h"

///3D REVISION
#include <SDL2/SDL_shape.h>
#include <glm/gtc/matrix_transform.hpp>

Sphere::Sphere( glm::vec3 a )
{
	x = a.x;
	y = a.y;
	z = a.z;
}

Sphere::Sphere( glm::vec3 a, float r )
{
	*this = a;
	this->r = r;
}

Sphere::Sphere(){}

Sphere Sphere::operator=( const glm::vec3& pos )
{
	x = pos.x;
	y = pos.y;
	z = pos.z;
	return *this;
}

Sphere Sphere::operator+( const glm::vec3& pos )
{
	x += pos.x;
	y += pos.y;
	z += pos.z;
	return *this;
}

Sphere Sphere::operator-( const glm::vec3& pos )
{
	x -= pos.x;
	y -= pos.y;
	z -= pos.z;
	return *this;
}

glm::mat4 Sphere::ct_mat( void )
{
	glm::mat4 base;
	base = glm::translate( base, glm::vec3( x, y, z ) );
	base = glm::scale( base, glm::vec3( r, r, r ) );
	return base;
}

bool collision::get_collide( const SDL_Rect &objA, const SDL_Rect &objB )
{
	if( objA.x > objB.x + objB.w )
		return false;
	if( objA.x + objA.w < objB.x )
		return false;
	if( objA.y > objB.y + objB.h )
		return false;
	if( objA.y + objA.h < objB.y )
		return false;
	return true;
}

bool collision::get_collide( const Sphere &a, const Sphere &b )
{
	float totalRadius = ( a.r + b.r );
	if( distance( a, b ) < totalRadius )
		return true;
	else
		return false;
}

bool collision::get_collide( const Sphere& a, const SDL_Rect& box )
{
	float closeX, closeY;
	if( a.x < box.x )
		closeX = box.x; // sets to left side
	else if( a.x > box.x + box.w )
		closeX = box.x + box.w; // sets to right side
	else
		closeX = a.x; //Sets the closest x point to the circles X since it is inside the box

	if( a.y < box.y )
		closeY = box.y; //top
	else if( a.y > box.y + box.h )
		closeY = box.y + box.h; //bottom
	else
		closeY = a.y; //inside

	if( distance( a.x, closeX, a.y, closeY ) < a.r )
		return true;
	else
		return false;
}

void collision::get_normal_diffXY( const SDL_Point& a, const SDL_Point& b, float* diffX, float* diffY )
{
	float dx = a.x - b.x;
	float dy = a.y - b.y;
	if( dx != 0 || dy != 0 )
	{
		float hypotonus = sqrt( dx * dx + dy * dy );
		dx /= hypotonus;
		dy /= hypotonus;

		if( diffX != NULL ) *diffX = dx;
		if( diffY != NULL ) *diffY = dy;
	}
	else
	{
		if( diffX != NULL ) *diffX = 0;
		if( diffY != NULL ) *diffY = 0;
	}
}
