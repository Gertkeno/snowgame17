#include <Texture.h>

#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_image.h>
#include <GL/glew.h>

#include <Shader.h>
using namespace gert;

#include <iostream>

#define DE_OUT( x ) std::cerr << "[Texture]" << x << std::endl;

bool Texture::linear_filter = true;

Texture::Texture( void )
{
	_textureIndex = 0;
	_sheetWidth = 1;
	_sheetHeight = 1;
	_width = _height = 0u;
}

Texture::~Texture( void )
{
	glDeleteTextures( 1, &_textureIndex );
}

bool Texture::file_load( const char* filename, uint16_t sw, uint16_t sh )
{
	_sheetWidth = 1;
	_sheetHeight = 1;
	SDL_Surface* tempSurf = IMG_Load( filename );
	if( tempSurf == nullptr )
	{
		DE_OUT( "[file_load]  This file could not be found or loaded: " << filename );
		return false;
	}

	bool co = load( (const uint8_t*)(tempSurf->pixels), tempSurf->format->BytesPerPixel, tempSurf->w, tempSurf->h );
	set_sheet_data( sw, sh );
	SDL_FreeSurface( tempSurf );
	return co;
}

bool Texture::RW_load( const void* RWmem, unsigned size )
{
	SDL_RWops *dataLoad = SDL_RWFromConstMem( RWmem, size );

	SDL_Surface* tempSurf = IMG_Load_RW( dataLoad, 1 );
	if( tempSurf == nullptr )
	{
		std::cerr << SDL_GetError() << std::endl;
		return false;
	}

	bool co = load( (const uint8_t*)(tempSurf->pixels), tempSurf->format->BytesPerPixel, tempSurf->w, tempSurf->h );
	SDL_FreeSurface( tempSurf );
	set_sheet_data( 1, 1 );
	return co;
}

bool Texture::load( const uint8_t* data, const int& mode, const int& width, const int& height )
{
	int ctMode = GL_RGBA;
	if( mode == 3 )
	{
		ctMode = GL_RGB;
	}
	else if( mode == 1 )
	{
		ctMode = GL_RED;
	}
	if( data == nullptr )
	{
		return false;
	}

	this->_width = width;
	this->_height = height;

	if( _textureIndex == 0 ) glGenTextures( 1, &_textureIndex );
	glBindTexture( GL_TEXTURE_2D, _textureIndex );
	//std::cerr << "Binding texture " << _textureIndex << std::endl;
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, ctMode, GL_UNSIGNED_BYTE, data );
	_set_parameters();
	return true;
}

void Texture::set_active( GLuint specific )
{
	glActiveTexture( GL_TEXTURE0 + specific );
	glBindTexture( GL_TEXTURE_2D, _textureIndex );

	char nameTex[ 5 ] = "tex";
	nameTex[ 3 ] = char( specific + '0' );
	nameTex[ 4 ] = '\0';//this limp execution only allows up to 10 ( 0-9 ) textures at a time;

	if( Shader::Active_Shader != nullptr )
	{
		GLint prog = Shader::Active_Shader->get_program();
		/*Shader::Active_Shader->set_uniform( "sheetWidth", (unsigned int)_sheetWidth );
		Shader::Active_Shader->set_uniform( "sheetHeight", (unsigned int)_sheetHeight );*/
		glUniform1i( glGetUniformLocation( prog, nameTex ), specific );
	}
	else
	{
		std::cerr << "[Texture][set_active]  ACTIVE SHADER NOT SET; location: " << this << std::endl;
	}
}

void Texture::_set_parameters()
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	auto filter = GL_LINEAR;
	if( !linear_filter )
		filter = GL_NEAREST;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
}

void Texture::set_sheet_data( uint16_t w, uint16_t h )
{
	if( w > 0 )
		_sheetWidth = w;
	if( h > 0 )
		_sheetHeight = h;
}

unsigned Texture::get_width()
{
	return _width;
}

unsigned Texture::get_height()
{
	return _height;
}

bool Texture::has_data() const
{
	return _textureIndex != 0;
}

GLuint Texture::get_texture_index() const
{
	return _textureIndex;
}
