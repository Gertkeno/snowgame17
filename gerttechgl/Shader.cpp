#include "Shader.h"

#include <SDL2/SDL_rwops.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

static constexpr uint8_t SHADER_RECENT_MAX = 30;
static constexpr uint8_t SHADER_RECENT_STR = 17;//total character, keep uniforms under SHADER_RECENT_STR characters long
static constexpr uint8_t SHADER_ERROR_MAX = 0xFF;

using namespace gert;

Shader* Shader::Active_Shader = nullptr;
Shader::Shader( void )
{
	_shaders = new GLuint[ TOTAL ];
	_program = new GLuint{0};
}

Shader::~Shader( void )
{
	destroy_me(); 
	delete[] _shaders;
	delete _program;
}

bool Shader::open_shader_file( const char* fileName, shadeType t )
{
	if( t == TOTAL )
	{
		GLuint i = 0;
		while( fileName[ i ] != '\0' )
		{
			if( fileName[ i++ ] != '.' ) continue;
			switch( fileName[ i ] )
			{
			case 'v':
				t = VERTEX;
				break;
			case 'f':
				t = FRAGMENT;
				break;
			}
			break;
		}
	}

	SDL_RWops* shadSource = SDL_RWFromFile( fileName, "r" );
	if( shadSource != nullptr && SDL_RWsize( shadSource ) > 0 )
	{
        unsigned int shadSize = SDL_RWsize( shadSource );
		GLchar* textBuffer = new GLchar[ shadSize + 1 ];
		SDL_RWread( shadSource, textBuffer, sizeof( GLchar ), shadSize );
		textBuffer[ shadSize ] = '\0';
		SDL_RWclose( shadSource );

		bool rval = open_shader( textBuffer, t );
		delete[] textBuffer;
		return rval;
	}
	else
	{
		std::cerr << "[Shader]  file could not be opened " << fileName << '\n';
	}
	return false;
}

bool Shader::attach_shaders( void )
{
	*_program = glCreateProgram();
	glAttachShader( *_program, _shaders[ FRAGMENT ] );
	glAttachShader( *_program, _shaders[ VERTEX ] );

	glLinkProgram( *_program );
	return true;
}

bool Shader::open_shader_file( const char* filename0, const char* filename1 )
{
	bool good = ( open_shader_file( filename0 ) && open_shader_file( filename1 ) );
	if( good )
		attach_shaders();

	return good;
}

bool Shader::open_shader( const char* source, shadeType t )
{
	if( source == nullptr or t == TOTAL )
		return false;

	_shaders[ t ] = glCreateShader( GL_FRAGMENT_SHADER + t );
	glShaderSource( _shaders[ t ], 1, &source, nullptr );
	glCompileShader( _shaders[ t ] );

	GLint status;
	glGetShaderiv( _shaders[ t ], GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE )
	{
		int len;
		glGetShaderiv( _shaders[t], GL_INFO_LOG_LENGTH, &len );
		std::cerr << "[Shader]  error making shader\n";
		if( len < 1 ) return false;
		char * text = new char[len];
		glGetShaderInfoLog( _shaders[ t ], SHADER_ERROR_MAX, nullptr, text );
		std::cerr << text << '\n';
		delete[] text;
		return false;
	}
	return true;
}

void Shader::use_shader( void )
{
	glUseProgram( *_program );
	Active_Shader = this;
}

void Shader::destroy_me( void )
{
	glDeleteProgram( *_program );
	glDeleteShader( _shaders[ FRAGMENT ] );
	glDeleteShader( _shaders[ VERTEX ] );
	if( Active_Shader == this )
	{
		Active_Shader = nullptr;
	}
}

GLuint Shader::get_program( void )
{
	return *_program;
}

void Shader::set_uniform( const std::string& title, float value )
{
	glProgramUniform1f( *_program, get_location( title ), value );
}

void Shader::set_uniform( const std::string& title, int value )
{
	glProgramUniform1i( *_program, get_location( title ), value );
}

void Shader::set_uniform( const std::string& title, GLuint value )
{
	glProgramUniform1ui( *_program, get_location( title ), value );
}

void Shader::set_uniform( const std::string& title, glm::vec3 value )
{
	glProgramUniform3fv( *_program, get_location( title ), 1, glm::value_ptr( value ) );
}

void Shader::set_uniform( const std::string& title, glm::vec4 value )
{
	glProgramUniform4fv( *_program, get_location( title ), 1, glm::value_ptr( value ) );
}

int Shader::get_location( const std::string& title )
{
	if( _recentUniform.count(title) == 0u )
	{
		_recentUniform[title] = glGetUniformLocation( *_program, title.c_str() );
		std::cerr << "[Shader]  Made new map entry, key " << title << " and value " << _recentUniform[title] << std::endl;
	}
	return _recentUniform[title];
}
