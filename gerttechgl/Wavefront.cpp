#include "Wavefront.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL_rwops.h>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <vector>

#include <Shader.h>

static const uint8_t OBJ_VERTS_PER_POINT = 3;
static const uint8_t OBJ_UV_PER_POINT = 2;
static const uint8_t OBJ_NORM_PER_POINT = 3;

GLint gert::Wavefront::mat_Attrib = 0;
GLint gert::Wavefront::normMat_Attrib = 0;

#define DE_OUT( x ) std::cout << "[Wavefront]" << x << std::endl

gert::Wavefront::Wavefront()
{
	//ctor
	vertices = nullptr;
	_totalVerts = 0u;
	_vertexArray = 0u;
	_arrayObject = 0u;
	_myData = flOBJ_TYPE(0);
	_allocatedData = false;
}

gert::Wavefront::~Wavefront()
{
	//dtor
	destroy_me();
}

bool gert::Wavefront::file_load( const char* fileName )
{
	SDL_RWops* inFile = SDL_RWFromFile( fileName, "r" );
	if( inFile == nullptr )
	{
		DE_OUT( "[file_load]  Wavefront file could not be found or loaded: " << fileName );
		return false;
	}
	GLuint sizeBuffer = SDL_RWsize( inFile );
	char* fileBuffer = new char[ sizeBuffer ];
	SDL_RWread( inFile, fileBuffer, sizeof( char ), sizeBuffer );
	SDL_RWclose( inFile );

	bool rval = file_parse( fileBuffer );
	delete[] fileBuffer;
	return rval;
}

bool gert::Wavefront::file_parse( const char* source )
{
	if( source == nullptr ) return false;

	struct FaceIndex
	{
		GLuint ver, tex, nor;
	};

	///READ
	_myData = flVECTOR;
	std::vector<float> _verts;
	std::vector<float> _uvs;
	std::vector<float> _norms;
	std::vector<FaceIndex> _faces;
	char header[0xFF];
	flOBJ_TYPE previousHead = flNONE;
	GLuint index = 0;

	while( source[ index ] != '\0' )
	{
		uint16_t hi = 0;
		while( source[ index ] != ' ' and source[ index ] != '\n' and source[ index ] != '\0' )
		{
			header[ hi ] = source[ index ];
			hi += 1;
			index += 1;
		}
		index += 1;
		header[ hi ] = '\0';

		if( header[ 0 ] == '#' )///comment check
		{
			previousHead = flNONE;
			while( source[ index ] != '\n' and source[ index ] != '\0' )
			{
				index++;
			}
			index++;
		}

		if( strcmp( header, "v" ) == 0 )///header check
		{
			previousHead = flVECTOR;
		}
		else if( strcmp( header, "vt" ) == 0 )
		{
			previousHead = flTEXTURE_UV;
		}
		else if( strcmp( header, "vn" ) == 0 )
		{
			previousHead = flNORMAL_VECTOR;
		}
		else if( strcmp( header, "f" ) == 0 )
		{
			previousHead = flFACES_INFO;
		}
		else
		{
			switch( previousHead )///Loading based on header check
			{
				case flVECTOR:
					_verts.push_back( strtod( header, nullptr ) );
					break;
				case flTEXTURE_UV:
					_uvs.push_back( strtod( header, nullptr ) );
					break;
				case flNORMAL_VECTOR:
					_norms.push_back( strtod( header, nullptr ) );
					break;
				case flFACES_INFO:
					{
						char* headAug;
						GLuint vertI( strtol( header, &headAug, 10 ) - 1 );
						GLuint uvI( 0 );
						GLuint normI( 0 );
						if( *headAug != '\0' and isdigit( *++headAug ) )//no texture index
						{
							uvI = strtol( headAug, &headAug, 10 ) - 1;
						}

						if( *headAug != '\0' and isdigit( *++headAug ) )//no normal index
						{
							normI = strtol( headAug, nullptr, 10 ) - 1;
						}

						_faces.push_back( { vertI, uvI, normI } );
					}
					break;
				case flNONE:
					break;
			}

			if( source[ index-1 ] == '\n' or source[ index-1 ] == '\0' )///newline remove previous header
			{
				previousHead = flNONE;
			}
		}///not a header
	}///while

	if( _faces.size() <= 0 )//no faces don't bother allocating float pointers
		return false;

	float* v = _verts.data();
	float* t = nullptr;
	float* n = nullptr;
	if( _uvs.size() > 0 )
		t = _uvs.data();
	if( _norms.size() > 0 )
		n = _norms.data();
	GLuint* f = (GLuint*)( _faces.data() );

	return obj_load( v, t, n, f, _faces.size() );
}

bool gert::Wavefront::obj_load( const float* verts, const float* texs, const float* norms, const GLuint* faces, GLuint total )
{
	destroy_me();
	if( verts == nullptr ) return false;
	if( texs != nullptr ) _myData = flOBJ_TYPE( _myData | flTEXTURE_UV );
	if( norms != nullptr ) _myData = flOBJ_TYPE( _myData | flNORMAL_VECTOR );

	_totalVerts = total;
	vertices = new FullVertex[ _totalVerts ];
	_allocatedData = true;

	static constexpr unsigned char FACE_SIZE = 3;
	bool useTex = _myData & flTEXTURE_UV;
	bool useNorm = _myData & flNORMAL_VECTOR;
	for( GLuint i = 0; i < _totalVerts; i++ )
	{
		//convert loaded index to stored offsets
		GLuint v = faces[ i * FACE_SIZE + 0 ] * OBJ_VERTS_PER_POINT;
		GLuint t = faces[ i * FACE_SIZE + 1 ] * OBJ_UV_PER_POINT;
		GLuint n = faces[ i * FACE_SIZE + 2 ] * OBJ_NORM_PER_POINT;
		vertices[ i ].x = verts[ v + 0 ];
		vertices[ i ].y = verts[ v + 1 ];
		vertices[ i ].z = verts[ v + 2 ];
		if( useTex )
		{
			vertices[ i ].tx = texs[ t + 0 ];
			vertices[ i ].ty = texs[ t + 1 ];
		}

		if( useNorm )
		{
			vertices[ i ].nx = norms[ n + 0 ];
			vertices[ i ].ny = norms[ n + 1 ];
			vertices[ i ].nz = norms[ n + 2 ];
		}
	}

	///Create vertex array
	glGenVertexArrays( 1, &_arrayObject );
	glBindVertexArray( _arrayObject );
	glGenBuffers( 1, &_vertexArray );
	glBindBuffer( GL_ARRAY_BUFFER, _vertexArray );
	glBufferData( GL_ARRAY_BUFFER, _totalVerts*sizeof(FullVertex), vertices, GL_STATIC_DRAW );

	//make this next part a function pointer
	GLuint vertSize = sizeof( FullVertex );
	auto shadeProg = Shader::Active_Shader->get_program();
	auto posat = glGetAttribLocation( shadeProg, "position" );
	glEnableVertexAttribArray( posat );
	glVertexAttribPointer( posat, OBJ_VERTS_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, nullptr );
	auto texat = glGetAttribLocation( shadeProg, "texcoord" );
	glEnableVertexAttribArray( texat );
	glVertexAttribPointer( texat, OBJ_UV_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, (void*)(3*sizeof(float)) );
	auto norat = glGetAttribLocation( shadeProg, "vnormal" );
	glEnableVertexAttribArray( norat );
	glVertexAttribPointer( norat, OBJ_NORM_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, (void*)(5*sizeof(float)) );

	//unbinding for little reason
	glBindVertexArray(0);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	return true;
}

bool gert::Wavefront::raw_verts_load( const float* verts, GLuint total )
{
	_totalVerts = total;
	vertices = (FullVertex*)(verts);
	_myData = flOBJ_TYPE( flVECTOR | flTEXTURE_UV | flNORMAL_VECTOR );

	///Create vertex array
	glGenVertexArrays( 1, &_arrayObject );
	glBindVertexArray( _arrayObject );
	glGenBuffers( 1, &_vertexArray );
	glBindBuffer( GL_ARRAY_BUFFER, _vertexArray );
	glBufferData( GL_ARRAY_BUFFER, _totalVerts*sizeof(FullVertex), vertices, GL_STATIC_DRAW );

	//make this next part a function pointer
	GLuint vertSize = sizeof( FullVertex );
	auto shadeProg = Shader::Active_Shader->get_program();
	auto posat = glGetAttribLocation( shadeProg, "position" );
	glEnableVertexAttribArray( posat );
	glVertexAttribPointer( posat, OBJ_VERTS_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, nullptr );
	auto texat = glGetAttribLocation( shadeProg, "texcoord" );
	glEnableVertexAttribArray( texat );
	glVertexAttribPointer( texat, OBJ_UV_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, (void*)(3*sizeof(float)) );
	auto norat = glGetAttribLocation( shadeProg, "vnormal" );
	glEnableVertexAttribArray( norat );
	glVertexAttribPointer( norat, OBJ_NORM_PER_POINT, GL_FLOAT, GL_FALSE, vertSize, (void*)(5*sizeof(float)) );

	//unbinding for little reason
	glBindVertexArray(0);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	return true;
}

#ifdef _GERT_DEBUG
void gert::Wavefront::print( void )
{
	std::cout << "[Wavefront][" << this << "]  raw vert data\n";
	for( GLuint i = 0; i < _totalVerts; i++ )
	{
		float *v = (float*)(&vertices[ i ]);
		for( GLuint a = 0; a < sizeof( FullVertex ) / sizeof( float ); a++ )
		{
			std::cout << *( v + a ) << ",\t";//pointers are great
		}
		std::cout << std::endl;
	}///this read can take a long time cout-ing
	std::cout << "OBJ totalVerts: " << _totalVerts << std::endl;
}
#endif // _GERT_DEBUG

void gert::Wavefront::draw( const glm::mat4& matrix4, const glm::vec4& colorMod, const uint16_t& frame )
{
	if( Shader::Active_Shader != nullptr )
	{
		Shader::Active_Shader->set_uniform( "colorMod", colorMod );
		//Shader::Active_Shader->set_uniform( "sheetFrame", (unsigned int)frame );
	}

	glBindVertexArray( _arrayObject ); //should point to buffer data, and how said data is stored

	glUniformMatrix4fv( mat_Attrib, 1, GL_FALSE, glm::value_ptr( matrix4 ) );

	if( _myData & flNORMAL_VECTOR )
	{
		glm::mat4 nmatrix = glm::transpose( glm::inverse( matrix4 ) );
		glUniformMatrix4fv( normMat_Attrib, 1, GL_FALSE, glm::value_ptr( nmatrix ) );
	}

	glDrawArrays( GL_TRIANGLES, 0, _totalVerts ); ///drawArrays( 1, 2, count ) count wants total as indexs not how many floats
	glBindVertexArray(0);
}

void gert::Wavefront::destroy_me( void )
{
	glDeleteBuffers( 1, &_vertexArray );
	glDeleteVertexArrays( 1, &_arrayObject );
	_totalVerts = 0;
	_myData = flOBJ_TYPE(0);
	if( vertices != nullptr and _allocatedData )
	{
		delete[] vertices;
	}
	vertices = nullptr;
	_allocatedData = false;
}

bool gert::Wavefront::has_data( void )
{
	return vertices != nullptr  and _vertexArray != 0;
}

GLuint gert::Wavefront::get_total_verts()
{
	return _totalVerts;
}

void gert::Wavefront::update_attribs( void )
{
	GLuint shaderProg = Shader::Active_Shader->get_program();
	mat_Attrib = glGetUniformLocation( shaderProg, "model" );
	normMat_Attrib = glGetUniformLocation( shaderProg, "normMod" );
}
