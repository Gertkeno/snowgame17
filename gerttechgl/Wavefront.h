#ifndef GERTWAVEFRONT_H
#define GERTWAVEFRONT_H

#include <glm/detail/type_mat4x4.hpp>
#include <glm/detail/type_vec4.hpp>

typedef unsigned int GLuint;
typedef int GLint;
typedef unsigned short int uint16_t;
typedef unsigned char uint8_t;

namespace color
{
	const glm::vec4 WHITE  ={ 1.0f, 1.0f, 1.0f, 1.0f };
	const glm::vec4 RED    ={ 1.0f, 0.0f, 0.0f, 1.0f };
	const glm::vec4 GREEN  ={ 0.0f, 1.0f, 0.0f, 1.0f };
	const glm::vec4 BLUE   ={ 0.0f, 0.0f, 1.0f, 1.0f };
	const glm::vec4 BLACK  ={ 0.0f, 0.0f, 0.0f, 1.0f };
	const glm::vec4 CYAN   ={ 0.0f, 1.0f, 1.0f, 1.0f };
	const glm::vec4 MAGENTA={ 1.0f, 0.0f, 1.0f, 1.0f };
	const glm::vec4 YELLOW ={ 1.0f, 1.0f, 0.0f, 1.0f };
}

namespace gert
{
	struct FullVertex
	{
		float x, y, z, tx, ty, nx, ny, nz;
	};

	class Wavefront
	{
		public:
			Wavefront();
			virtual ~Wavefront();

			bool file_load( const char* filename );//uses file_parse
			bool file_parse( const char* source );//uses obj_load
			bool obj_load( const float* verts, const float* texs, const float* norms, const GLuint* faces, GLuint total );
			bool raw_verts_load( const float* verts, GLuint total );
			bool has_data( void );
			void destroy_me( void );

			void draw( const glm::mat4& modelMatrix, const glm::vec4& colorMod, const uint16_t& frame = 0 );
#ifdef _GERT_DEBUG
			void print(void);///< bad function, only for debugging; couts every vertex, uv, and normal
#endif // _GERT_DEBUG

			FullVertex* vertices;
			GLuint get_total_verts();

			static void update_attribs( void );
		protected:
			static GLint mat_Attrib, normMat_Attrib;//find a way to get rid of these

			GLuint _totalVerts;
			GLuint _vertexArray;
			GLuint _arrayObject;
			enum flOBJ_TYPE: uint8_t
			{
				flVECTOR = 1 << 0,
				flTEXTURE_UV = 1 << 1,
				flNORMAL_VECTOR = 1 << 2,
				flFACES_INFO = 1 << 3,
				flNONE = 1 << 4,
			};
			flOBJ_TYPE _myData;

			bool _allocatedData;
	};

}

#endif // GERTWAVEFRONT_H
