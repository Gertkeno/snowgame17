#ifndef COLLISION_H
#define COLLISION_H

struct SDL_Rect;
struct SDL_Point;

#include <cmath>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>
///3D GL REVISION

struct Sphere
{
	float x, y, z, r;
	Sphere( glm::vec3 );
	Sphere( glm::vec3, float r );
	Sphere();
	glm::mat4 ct_mat( void );
	Sphere operator=( const glm::vec3& pos );
	Sphere operator+( const glm::vec3& pos );
	Sphere operator-( const glm::vec3& pos );
};

namespace collision
{

	//double distance( const float&, const float&, const float&, const float& );
	template<typename kind>
		double distance( const kind &x1, const kind &x2, const kind &y1, const kind &y2 )
		{
			kind distX = x2 - x1;
			kind distY = y2 - y1;
			return sqrt( ( distY * distY ) + ( distX * distX ) );
		}

	template<typename kinda, typename kindb>
		double distance( const kinda& t1, const kindb& t2 )
		{
			double distX = t1.x - t2.x;
			double distY = t1.y - t2.y;
			double distZ = t1.z - t2.z;
			return sqrt( ( distY * distY ) + ( distX * distX ) + ( distZ * distZ ) );
		}

	bool get_collide( const SDL_Rect& objA, const SDL_Rect& objB );
	//Circle collisions
	bool get_collide( const Sphere&, const Sphere& );
	bool get_collide( const Sphere& a, const SDL_Rect& box );

	void get_normal_diffXY( const SDL_Point& a, const SDL_Point& b, float* diffX, float* diffY );
}

#endif // COLLISION_H
