#ifndef GERT_TEXTURE_H
#define GERT_TEXTURE_H

typedef unsigned int GLuint;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

namespace gert
{
	class Shader;

	class Texture
	{
		public:
			Texture( void );
			virtual ~Texture( void );

			bool file_load( const char* filename, uint16_t sw = 1, uint16_t sh = 1 );
			bool load( const uint8_t* data, const int& mode, const int& width, const int& height );///< uncompressed data to texture
			bool RW_load( const void* RWmem, unsigned size );
			void set_active( GLuint specific = 0 );
			void set_sheet_data( uint16_t w, uint16_t h );

			bool has_data() const;
			unsigned get_width();
			unsigned get_height();

			GLuint get_texture_index() const;

			static bool linear_filter;
		protected:
			GLuint _textureIndex;
			uint16_t _sheetWidth, _sheetHeight;
			void _set_parameters();
			unsigned _width, _height;
	};

}
#endif // GERT_TEXTURE_H
