namespace FrameTime
{
	double get();
	void update();
	unsigned since_update();
}
