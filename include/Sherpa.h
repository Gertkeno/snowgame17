#ifndef SHERPA_H
#define SHERPA_H

#include <Player.h>

class Sherpa: public Player
{
	public:
		Sherpa();
		~Sherpa();

		void update();
		void draw();

	protected:
		//
};

#endif
