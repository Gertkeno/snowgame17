#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
namespace GameManager
{
	void draw();
	void update();

	enum state_t
	{
		PLAYING,
		QUITTING,
	};

	state_t get_game_state();

	void init();
	void destroy();
}
#endif //GAMEMANAGER_H
