#ifndef STAGE_H
#define STAGE_H
#include <stddef.h>
#include <geo/Line.h>
#include <geo/Circle.h>
namespace Stage
{
	bool load_stage( const void * bg, size_t bgsize, const geometry::Line * data, size_t size, glm::vec2 = {1.0f,1.0f});
	glm::vec2 get_force( const geometry::Circle& );

	void draw();
}
#endif
