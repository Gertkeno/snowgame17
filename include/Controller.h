#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>
#include <map>
#include <vector>
#include <functional>
#include <SDL2/SDL_events.h>
#include <Input.h>

class Controller
{
	public:
		Controller();
		~Controller();

		void manage_inputs( const SDL_Event* );

		float get_input ( std::string name ) const;
		float operator[]( std::string name ) const;//same as .get_input
		bool  get_press ( std::string name ) const;
		bool  operator()( std::string name ) const;//same as .get_press
		void add_input( std::string name, Input in );

		bool joy_add( int deviceIndex );
		bool joy_remove( SDL_JoystickID instanceID );
		int joy_count() const;

		bool load_file( const char * filename );
		bool save_file( const char * filename ) const;
		bool load_raw( std::pair<std::string, Input> * data, size_t );
		bool mutate_value( std::string, Input );

		void reset_press();
		void reset_held();
	private:
		std::map<std::string, Input> _data;
		std::vector<SDL_Joystick*> _joys;

		bool _tracked_joystick( SDL_JoystickID );
		void _search_input( Input::type_t, int, std::function<void(Input*)> );
};

#endif//CONTROLLER_H
