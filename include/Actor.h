#ifndef ACTOR_H
#define ACTOR_H

#include <glm/glm.hpp>
class Actor
{
	public:
		Actor();
		virtual ~Actor();
		
		glm::vec3 get_world_pos() const;
	protected:
		glm::vec3 _worldPos;
};

#endif //ACTOR_H
