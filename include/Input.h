#ifndef INPUT_H
#define INPUT_H

struct Input
{
	enum struct type_t: char{KEYBOARD, JOYBUTTON, JOYAXIS, MOUSE, NONE} myType;

	long int button;//SDL_keycode, button, mousebutton or axis id
	short int axisMax; //just for axis
	bool axisPass; //passed framePress threshold

	float held;//0.0 to 1.0
	bool framePress;
};

#endif//INPUT_H
