#ifndef PLAYER_H
#define PLAYER_H

#include <Actor.h>

class Player: public Actor
{
	public:
		Player();
		virtual ~Player();

		virtual void update();//should handle basic movement
		virtual void draw();

		virtual void start( float x, float y );
		static constexpr float SCALE{0.01f};
	protected:
		enum state_t
		{
			GROUNDED,
			FALLING,
			CLIMBING,
			TOTAL,
		} _status;
		glm::vec2 _velocity;

		glm::vec2 _turn;
};

#endif
