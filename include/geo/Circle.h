#ifndef GEO_CIRCLE_H
#define GEO_CIRCLE_H

#include <glm/glm.hpp>
namespace geometry
{
	struct Circle
	{
		glm::vec2 pos;
		float radius;
	};
}
#endif
