#ifndef GEO_FUNCTIONAL_H
#define GEO_FUNCTIONAL_H

#include <glm/glm.hpp>
#include <geo/Line.h>
#include <geo/Circle.h>

namespace geometry
{
	glm::vec2 line_intersection( const Line& A, const Line& B );
	glm::vec2 intersection_force( const Circle& obj, const Line& wall );
	
	template<typename C>
	C distance( glm::vec2 a, glm::vec2 b )
	{
		return sqrt( pow( a.x - b.x, 2 ) + pow( a.y - b.y, 2 ) );
	}
}

#endif
