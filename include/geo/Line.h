#ifndef GEO_LINE_H
#define GEO_LINE_H
#include <glm/glm.hpp>

namespace geometry
{
	struct Line
	{
		glm::vec2 a, b;
		bool flip=false;

		float length() const;
		float slope() const;
		float y_offset() const;
		float height( float x ) const;

		float high_x() const;
		float low_x() const;
		float high_y() const;
		float low_y() const;

		bool y_parallel() const;
		bool x_parallel() const;
	};
}

#endif //GEO_LINE_H
