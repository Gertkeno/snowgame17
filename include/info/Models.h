#ifndef INFO_MODELS_H
#define INFO_MODELS_H
#include <Wavefront.h>

namespace model
{
	enum
	{
		CUBE,
		TEST_MONKEY,
		PLANE,
		TOTAL
	};
}

extern gert::Wavefront* gModel;

#endif
